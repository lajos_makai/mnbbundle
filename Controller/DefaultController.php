<?php
/*
* This file is part of the MakaiMNBBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\MNBBundle\Controller;

use Makai\MNBBundle\Entity\MnbCurrency;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    
	 /**
     * Az aktuális árfolyam lekérdezése az mnb oldaláról, és a kapott válasz elmentése adatbázisba
     *
     */
	public function indexAction($currency, $user, $password)
    {
    	if($user != $this->container->getParameter('mnb_user') || $password != $this->container->getParameter('mnb_password')){
    		throw $this->createNotFoundException('Nincs hozzaferese az arfolyam frissiteshez.');
    	}
    	
        $client = new \SoapClient($this->container->getParameter('mnb_soap_client'));
        $response = $client->__soapCall("GetCurrentExchangeRates", array());
        $doc = new \DOMDocument;
        $doc->loadXML($response->GetCurrentExchangeRatesResult);
        $xpath = new \DOMXPath($doc);
        $query = "//MNBCurrentExchangeRates/Day/Rate[@curr='$currency']";
        $entries = $xpath->query($query);
        /*ha jött válasz, akkor elmentjük db-be az adatokat*/
        if($entries->length){
            try{
                $mnb  = new MnbCurrency();

                $now = new \DateTime();

                $mnb->setValue( str_replace(",", ".",$entries->item(0)->nodeValue ));
                $mnb->setDate($now);
                $mnb->setCurrency($currency);

                $em = $this->getDoctrine()->getManager();

                $em->persist($mnb);
                $em->flush();
            }catch (Exception $e) {
                throw $this->createNotFoundException('Hiba a PosPaymentTransaction mentése során');
            }

            return new JsonResponse(array($currency=>$entries->item(0)->nodeValue));
        } else {
            throw $this->createNotFoundException('Nem sikerült az MNB adatok letöltése');
        }

    }

}
