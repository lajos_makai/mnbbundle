<?php
/*
* This file is part of the MakaiMNBBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\MNBBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('makai_mnb');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $rootNode
            ->children()
            ->scalarNode('mnb_user')
            ->defaultValue('makai')
            ->info('felhasználónév az importálás futtatásához')
            ->end()
            ->scalarNode('mnb_password')
            ->defaultValue('654321')
            ->info('jelszó az importálás futtatásához')
            ->end()
            ->scalarNode('mnb_soap_client')
            ->defaultValue('http://www.mnb.hu/arfolyamok.asmx?wsdl')
            ->info('MNB SOAP kliens')
            ->end()
            ->end()
        ;
        return $treeBuilder;
    }
}
