## Telepítés ##

### For Symfony >= 2.3.* ###

Fel kell venni a composer.json fájlba:

```
{
    "require": {
        "makai/mnb-bundle": "dev-master",
    }
     "repositories": [
        {
            "type": "vcs",
            "url": "https://lajos_makai@bitbucket.org/lajos_makai/mnbbundle.git"
        }
    ]
}
```
### Az alábbi modulok kell telepíteni ###
```
knplabs/doctrine-behaviors
```

### Bundle telepítése ###

```
$ php composer.phar update makai/mnb-bundle
```

### Bundle regisztrálása###

```php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        new Makai\MNBBundle\MakaiMNBBundle(),
    );
}
```

app/routing.yml fájlba fel kell venni:

```
# MakaiMNBBundle
makai_mnb:
    resource: "@MakaiMNBBundle/Resources/config/routing.yml"
    prefix:   /mbn
```

### Bundle paraméterezési lehetőségei ###
```
#az mbn árfolyam frissítéséhez szükséges adatok
mnb_user:  makai
mnb_password:  654321
mnb_soap_client: http://www.mnb.hu/arfolyamok.asmx?wsdl
```



### Adatbázis schema frissítése ###
```
php app/console doctrine:schema:update --force
```
### Példa a legfrisebb árfolyam lekérdezésére (pl.: USD) ###
```
        /*a $ aktuális árfolyamának lekérdezése*/
        $em = $this->getDoctrine()->getManager();
        $mnbCurrency = $em->getRepository('MakaiMNBBundle:MnbCurrency')
            ->findCurrencyActualValue('USD');
```            