<?php
/*
* This file is part of the MakaiMNBBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\MNBBundle\Entity;

use Doctrine\ORM\EntityRepository;

class MnbCurrencyRepository extends EntityRepository
{
    /*visszaadja a paraméterként kapott deviza aktuális árfolyamát*/
    public function findCurrencyActualValue($currency)
    {

        $query = $this->createQueryBuilder('mc')
            ->where('mc.currency = :currency')
            ->setParameter('currency', $currency)
            ->orderBy('mc.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery();

        return $query->getOneOrNullResult();

    }
}