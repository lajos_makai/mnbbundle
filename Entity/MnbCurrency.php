<?php
/*
* This file is part of the MakaiMNBBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\MNBBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MnbCurrency
 *
 * @ORM\Table(name="mnb_currency")
 * @ORM\Entity(repositoryClass="Makai\MNBBundle\Entity\MnbCurrencyRepository")
 */
class MnbCurrency
{
    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=11, nullable=false)
     */
    private $currency;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="float", nullable=false)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;



    /**
     * Set currency
     *
     * @param string $currency
     * @return MnbCurrency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    
        return $this;
    }

    /**
     * Get currency
     *
     * @return string 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return MnbCurrency
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return MnbCurrency
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}