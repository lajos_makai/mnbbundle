<?php
/*
* This file is part of the MakaiMNBBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/
namespace Makai\MNBBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MakaiMNBBundle extends Bundle
{
}
